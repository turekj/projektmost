int red = 3;
int yellow = 4;
int green = 5;

int fotorezistor = A0;
int light = 0;

int button = 2;
boolean stav = true;

int lamp = 6;

#define STEPPER_PIN_1 9
#define STEPPER_PIN_2 10
#define STEPPER_PIN_3 11
#define STEPPER_PIN_4 12

int step_number = 0;

void setup()
{
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(fotorezistor, INPUT);
  pinMode(button, INPUT);
  pinMode(STEPPER_PIN_1, OUTPUT);
  pinMode(STEPPER_PIN_2, OUTPUT);
  pinMode(STEPPER_PIN_3, OUTPUT);
  pinMode(STEPPER_PIN_4, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(button),zmena,FALLING);
  Serial.begin(9600);
}

void zmena(){
  stav = !stav;
}

void dole(){
  digitalWrite(green, HIGH);
  digitalWrite(yellow, LOW);
  digitalWrite(red, LOW);
}

void nahore(){
  digitalWrite(green, LOW);
  digitalWrite(yellow, HIGH);
  delay(2000);
  
  digitalWrite(yellow, LOW);
  digitalWrite(red, HIGH);
  delay(2000);
  
  
}
  
  
void nahoree(){
  delay(2000);
      
    digitalWrite(yellow, HIGH);
    
    delay(1000);
    
    
  
}
void OneStep(bool dir){
  
    if(dir){
  switch(step_number){
    case 0:
    digitalWrite(STEPPER_PIN_1, HIGH);
    digitalWrite(STEPPER_PIN_2, LOW);
    digitalWrite(STEPPER_PIN_3, LOW);
    digitalWrite(STEPPER_PIN_4, LOW);
    break;
    case 1:
    digitalWrite(STEPPER_PIN_1, LOW);
    digitalWrite(STEPPER_PIN_2, HIGH);
    digitalWrite(STEPPER_PIN_3, LOW);
    digitalWrite(STEPPER_PIN_4, LOW);
    break;
    case 2:
    digitalWrite(STEPPER_PIN_1, LOW);
    digitalWrite(STEPPER_PIN_2, LOW);
    digitalWrite(STEPPER_PIN_3, HIGH);
    digitalWrite(STEPPER_PIN_4, LOW);
    break;
    case 3:
    digitalWrite(STEPPER_PIN_1, LOW);
    digitalWrite(STEPPER_PIN_2, LOW);
    digitalWrite(STEPPER_PIN_3, LOW);
    digitalWrite(STEPPER_PIN_4, HIGH);
    break;
  } 
    }else{
      switch(step_number){
    case 0:
    digitalWrite(STEPPER_PIN_1, LOW);
    digitalWrite(STEPPER_PIN_2, LOW);
    digitalWrite(STEPPER_PIN_3, LOW);
    digitalWrite(STEPPER_PIN_4, HIGH);
    break;
    case 1:
    digitalWrite(STEPPER_PIN_1, LOW);
    digitalWrite(STEPPER_PIN_2, LOW);
    digitalWrite(STEPPER_PIN_3, HIGH);
    digitalWrite(STEPPER_PIN_4, LOW);
    break;
    case 2:
    digitalWrite(STEPPER_PIN_1, LOW);
    digitalWrite(STEPPER_PIN_2, HIGH);
    digitalWrite(STEPPER_PIN_3, LOW);
    digitalWrite(STEPPER_PIN_4, LOW);
    break;
    case 3:
    digitalWrite(STEPPER_PIN_1, HIGH);
    digitalWrite(STEPPER_PIN_2, LOW);
    digitalWrite(STEPPER_PIN_3, LOW);
    digitalWrite(STEPPER_PIN_4, LOW);
  
    
  } 
    }
  step_number++;
    if(step_number > 3){
      step_number = 0;
    }

  } 


void lampsOn(){
  digitalWrite(lamp, HIGH);
}

void lampsOff(){
  digitalWrite(lamp, LOW);
}

void loop()
{
  for(int i = 0; i <5;i++){
      //OneStep(false);
      delay(2);
  }
  
  light = analogRead(fotorezistor);
  
  
  if(light < 512){
    lampsOn();
    
  }else{
  	lampsOff();
    
  }
  
  if(stav){
   	dole(); 
  }else{
   	nahore();
      for(int i = 0; i <5500;i++){
        OneStep(true);
        delay(2);
      } 
      delay(5000);
      for(int i = 0; i <5500;i++){
      OneStep(false);
      delay(2);
      }
    nahoree();
    
  }
  stav = true;
}