# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 20-30h |
| jak se mi to podařilo rozplánovat |  dobře |
| návrh designu | https://gitlab.spseplzen.cz/turekj/projektmost/-/blob/main/dokumentace/design/nacrtek_situace.png |
| proč jsem zvolil tento design | nevím |
| zapojení | https://gitlab.spseplzen.cz/turekj/projektmost/-/blob/main/dokumentace/design/tinkercad.png |
| z jakých součástí se zapojení skládá | led, kabely, rezistory, fotorezistor, tlačítko, krokový motor |
| realizace | https://gitlab.spseplzen.cz/turekj/projektmost/-/blob/main/dokumentace/fotky/hotovy_produkt.jpg |
| jaký materiál jsem použil a proč | polystyrem - snadno tvarovatelný, 3dTisk, dřevo |
| co se mi povedlo | most |
| co se mi nepovedlo/příště bych udělal/a jinak | ---- |
| zhodnocení celé tvorby | za 1 |